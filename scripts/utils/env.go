package utils

import (
	"log"
	"os"
)

func GetGitlabToken() string {
	gitlabToken := os.Getenv("GITLAB_USER_TOKEN")
	if gitlabToken == "" {
		log.Fatalln("ERROR: GITLAB_USER_TOKEN is empty!")
	}
	return gitlabToken
}

func GetProjectOwner() string {
	projectOwner := os.Getenv("PROJECT_OWNER")
	if projectOwner == "" {
		log.Fatalln("ERROR: PROJECT_OWNER is empty!")
	}
	return projectOwner
}

func GetIACProjectID() string {
	iacProject := os.Getenv("CI_PROJECT_ID")
	if iacProject == "" {
		log.Fatalln("ERROR: CI_PROJECT_ID is empty!")
	}
	return iacProject
}
