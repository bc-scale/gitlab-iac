package main

import (
	"gitlab-iac/scripts/projects"
	"gitlab-iac/scripts/utils"
	"log"
	"os"
	"strings"
)

func main() {
	if len(os.Args) < 2 || os.Args[1] == "help" {
		help()
		return
	}
	cmd := os.Args[1]
	args := strings.TrimSpace(strings.Join(os.Args[2:], " "))

	if args == "" {
		workingDirectory, err := os.Getwd()
		if err == nil && strings.Contains(strings.TrimSuffix(workingDirectory, "/"), utils.GetRepoDir()) {
			args = strings.TrimPrefix(workingDirectory, utils.GetRepoDir())
		}
	}

	switch cmd {
	case "new", "init":
		projects.Init(args)
	case "list", "ls":
		log.Println("PROJECTS")
		for _, project := range utils.GetProjectFolders() {
			log.Println("=>", project)
		}
	case "diff", "changed":
		log.Println("CHANGED")
		changes, err := utils.GetChangedProjects()
		if err != nil {
			log.Fatalln(err)
		}
		if len(changes) > 0 {
			for _, project := range changes {
				log.Println("=>", project)
			}
		} else {
			log.Println("=> no changes")
		}
	case "template", "tmpl":
		projects.Template(args)
	case "import":
		projects.Import(args)
	case "plan":
		projects.Plan(args)
	case "apply":
		projects.Apply(args)
	}
	log.Println("COMPLETED")
}

func help() {
	log.Println("COMMANDS" +
		"\n=> help     - show this help text" +
		"\n=> new      - initialize project" +
		"\n=> list     - list all projects" +
		"\n=> diff     - list changed projects since master" +
		"\n=> import   - import existing states" +
		"\n=> template - generate template" +
		"\n=> plan     - terragrunt plan" +
		"\n=> apply    - terragrunt apply")
}
