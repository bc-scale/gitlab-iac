package projects

import (
	"bufio"
	"encoding/json"
	"fmt"
	"gitlab-iac/scripts/models"
	"gitlab-iac/scripts/utils"
	"io/ioutil"
	"log"
	"os"
	"regexp"
	"strings"
)

var REGEX_TERRAFORM_CLEANUP = regexp.MustCompile(`(?m)((^.*Refreshing state.*\n\n?)|\n(Saved the plan)(.|\n)*|(\n\n.*\n.*no differences.*)|((.|\n)*following actions:\n\n))`)

var MEMBERS = "MEMBERS"
var GROUPS = "GROUPS"
var BRANCH_PROTECTIONS = "BRANCH_PROTECTIONS"
var MR_APPROVALS = "MR_APPROVALS"
var VARIABLES = "VARIABLES"
var PIPELINE_SCHEDULES = "PIPELINE_SCHEDULES"
var MIRRORS = "MIRRORS"
var DEPLOY_KEYS = "DEPLOY_KEYS"

func Init(args string) {
	log.Println("INITIALIZING", args)
	if args == "" {
		log.Panic("ERROR: please specify a repository name as argument!")
	}
	_init(args)
}
func _init(project string) {
	projectDir := utils.GetRepoDir() + project
	if _, err := os.Stat(projectDir); os.IsNotExist(err) {
		_, err = utils.Cmd(utils.GetRepoDir(), "cp", "-r", ".template", project)
		if err != nil {
			log.Panicf("ERROR: unable to create project folder: %s\n%s", projectDir, err)
		}
		log.Println("=> initialized, delete `.terraignore` (y/n)? ")
		print("input: ")
		input := bufio.NewScanner(os.Stdin)
		input.Scan()
		if strings.ToLower(input.Text()) == "y" {
			_ = os.Remove(projectDir + "/.terraignore")
		}
	} else {
		log.Println("=> already initialized")
	}
}

func Template(args string) {
	log.Println("TEMPLATING", args)
	if args != "" {
		_template(args)
		return
	}
	changes, err := utils.GetChangedProjects()
	if err != nil {
		log.Panic(err)
	}
	if len(changes) == 0 {
		log.Println("=> no changes")
		return
	}
	for _, project := range changes {
		_template(project)
	}
}
func _template(project string) {
	log.Println("=> templating:", project)
	projectDir := utils.GetRepoDir() + project
	read, err := ioutil.ReadFile(projectDir + "/definition.yaml")
	if err != nil {
		log.Panic("ERROR: unable to read definitions\n", err)
		return
	}
	definition := string(read)
	_ = os.Setenv("EXAMPLE_ENV_VARIABLE", " ")
	r := regexp.MustCompile(`\${.+?}`)
	for _, old := range r.FindAllString(definition, -1) {
		env := strings.TrimSuffix(strings.TrimPrefix(old, "${"), "}")
		log.Println("===> replacing:", env)
		replacement := os.Getenv(env)
		if replacement == "" {
			log.Panic("=====> NOT FOUND")
		}
		definition = strings.Replace(definition, old, replacement, 1)
	}
	err = ioutil.WriteFile(projectDir+"/final.yaml", []byte(definition), 0600)
	if err != nil {
		log.Panic("ERROR: unable to write definitions\n", err)
	}
}

func Import(args string) {
	logs := &[]string{}
	defer func() {
		recover()
		log.Println("\n" + strings.Join(*logs, "\n"))
	}()
	log.Println("IMPORTING", args)
	if args != "" {
		_import(logs, args)
		return
	}
	changes, err := utils.GetChangedProjects()
	if err != nil {
		log.Panic(err)
	}
	if len(changes) == 0 {
		log.Println("=> no changes")
		return
	}
	ch := make(chan interface{}, len(changes))
	for _, project := range changes {
		go func(project string) {
			_import(logs, project)
			ch <- nil
		}(project)
	}
	for i := 0; i < len(changes); i++ {
		<-ch
	}
}
func _import(logs *[]string, project string) {
	projectDir := utils.GetRepoDir() + project
	if _, err := os.Stat(projectDir + "/final.yaml"); os.IsNotExist(err) {
		_template(project)
	}
	*logs = append(*logs, "=> importing: "+project)
	state := _importProject(logs, project, projectDir)
	if state.IsArchived() {
		*logs = append(*logs, "===> skipping archived project")
		return
	}
	projectID := state.GetProjectID()
	if projectID == "" {
		return
	}
	*logs = append(*logs, "===> project id: "+projectID)
	_importMembers(logs, project, projectDir, projectID)
	_importGroups(logs, project, projectDir, projectID)
	_importBranchProtections(logs, project, projectDir, projectID)
	_importMRApprovals(logs, project, projectDir, projectID)
	if state.IsPipelinesEnabled() {
		_importVariables(logs, project, projectDir, projectID)
		_importPipelineSchedules(logs, project, projectDir, projectID)
	}
	_importMirrors(logs, project, projectDir, projectID)
	_importDeployKeys(logs, project, projectDir, projectID)
}
func _importProject(logs *[]string, project string, projectDir string) models.State {
	_, err := utils.Cmd(projectDir, "terragrunt", "import", "gitlab_project.project", utils.GetProjectOwner()+"/"+project)
	if err != nil {
		if strings.Contains(err.Error(), "404") {
			*logs = append(*logs, "===> project not found, may be new, skipping...")
			return models.State{}
		} else if strings.Contains(err.Error(), "already managed") {
			*logs = append(*logs, "===> project already imported")
		} else {
			log.Panicf("ERROR: unable to run terragrunt import: project %s\n%s", project, err)
		}
	} else {
		*logs = append(*logs, "===> success")
	}
	stdout, err := utils.Cmd(projectDir, "terragrunt", "show", "-json")
	if err != nil {
		log.Panicf("ERROR: unable to run terragrunt show: project %s\n%s", project, err)
	}
	state, err := utils.StringToState(stdout)
	if err != nil {
		log.Panicf("ERROR: state invalid: project %s\n%s", project, err)
	}
	return state
}
func _importMembers(logs *[]string, project string, projectDir string, projectID string) {
	if utils.IsIgnored(projectDir, MEMBERS) {
		*logs = append(*logs, "===> members ignored")
		return
	}
	members, err := utils.GetGitlabProjectMembers(projectID)
	if err != nil {
		log.Panicf("ERROR: unable to get members from api: project %s\n%s", project, err)
	}
	for _, member := range members {
		*logs = append(*logs, fmt.Sprintf("===> processing member: %s", member.Username))
		if member.Username == os.Getenv("PROJECT_OWNER") {
			*logs = append(*logs, "=====> skipping owner")
			continue
		}
		_, err = utils.Cmd(projectDir, "terragrunt", "import",
			fmt.Sprintf("gitlab_project_membership.project_memberships[\"%s\"]", member.Username),
			fmt.Sprintf("%s:%d", projectID, member.ID))
		if err != nil {
			if !strings.Contains(err.Error(), "already managed") {
				log.Panicf("ERROR: unable to run terragrunt import: project %s member %s\n%s", project, member.Username, err)
			} else {
				*logs = append(*logs, "=====> member already imported")
			}
			continue
		}
		*logs = append(*logs, "=====> success")
	}
}
func _importGroups(logs *[]string, projectSlug string, projectDir string, projectID string) {
	if utils.IsIgnored(projectDir, GROUPS) {
		*logs = append(*logs, "===> groups ignored")
		return
	}
	project, err := utils.GetGitlabProject(projectID)
	if err != nil {
		log.Panicf("ERROR: unable to get groups from api: project %s\n%s", projectSlug, err)
	}
	for _, group := range project.SharedWithGroups {
		*logs = append(*logs, fmt.Sprintf("===> processing group: %s", group.GroupFullPath))
		_, err = utils.Cmd(projectDir, "terragrunt", "import",
			fmt.Sprintf("gitlab_project_share_group.project_groups[\"%s\"]", group.GroupFullPath),
			fmt.Sprintf("%s:%d", projectID, group.GroupID))
		if err != nil {
			if !strings.Contains(err.Error(), "already managed") {
				log.Panicf("ERROR: unable to run terragrunt import: project %s group %s\n%s", projectSlug, group.GroupFullPath, err)
			} else {
				*logs = append(*logs, "=====> group already imported")
			}
			continue
		}
		*logs = append(*logs, "=====> success")
	}
}
func _importBranchProtections(logs *[]string, project string, projectDir string, projectID string) {
	if utils.IsIgnored(projectDir, BRANCH_PROTECTIONS) {
		*logs = append(*logs, "===> branch protections ignored")
		return
	}
	protectedBranches, err := utils.GetGitlabProjectProtectedBranches(projectID)
	if err != nil {
		log.Panicf("ERROR: unable to get branch protections from api: project %s\n%s", project, err)
	}
	for _, protectedBranch := range protectedBranches {
		*logs = append(*logs, fmt.Sprintf("===> processing branch protection: %s", protectedBranch.Name))
		_, err = utils.Cmd(projectDir, "terragrunt", "import",
			fmt.Sprintf("gitlab_branch_protection.project_branch_protections[\"%s\"]", protectedBranch.Name),
			fmt.Sprintf("%s:%s", projectID, protectedBranch.Name))
		if err != nil {
			if !strings.Contains(err.Error(), "already managed") {
				log.Panicf("ERROR: unable to run terragrunt import: project %s branch protection %s\n%s", project, protectedBranch.Name, err)
			} else {
				*logs = append(*logs, "=====> branch protection already imported")
			}
			continue
		}
		*logs = append(*logs, "=====> success")
	}
}
func _importMRApprovals(logs *[]string, project string, projectDir string, projectID string) {
	if utils.IsIgnored(projectDir, MR_APPROVALS) {
		*logs = append(*logs, "===> MR approvals ignored")
		return
	}
	*logs = append(*logs, fmt.Sprintf("===> processing MR approvals: %s", project))
	_, err := utils.Cmd(projectDir, "terragrunt", "import",
		"gitlab_project_level_mr_approvals.project_mr_approvals",
		projectID)
	if err != nil {
		if !strings.Contains(err.Error(), "already managed") {
			log.Panicf("ERROR: unable to run terragrunt import: project %s MR approvals\n%s", project, err)
		} else {
			*logs = append(*logs, "=====> MR approvals already imported")
		}
	}
	*logs = append(*logs, "=====> success")
}
func _importVariables(logs *[]string, project string, projectDir string, projectID string) {
	if utils.IsIgnored(projectDir, VARIABLES) {
		*logs = append(*logs, "===> variables ignored")
		return
	}
	variables, err := utils.GetGitlabVariables(projectID)
	if err != nil {
		log.Panicf("ERROR: unable to get variables from api: project %s\n%s", project, err)
	}
	for _, variable := range variables {
		*logs = append(*logs, fmt.Sprintf("===> processing variable: %s (%s)", variable.Key, variable.EnvironmentScope))
		_, err = utils.Cmd(projectDir, "terragrunt", "import",
			fmt.Sprintf("gitlab_project_variable.project_variable[\"%s\"]", variable.Key),
			fmt.Sprintf("%s:%s:%s", projectID, variable.Key, variable.EnvironmentScope))
		if err != nil {
			if !strings.Contains(err.Error(), "already managed") {
				log.Panicf("ERROR: unable to run terragrunt import: project %s variable %s (%s)\n%s", project, variable.Key, variable.EnvironmentScope, err)
			} else {
				*logs = append(*logs, "=====> variable already imported")
			}
			continue
		}
		*logs = append(*logs, "=====> success")
	}
}
func _importPipelineSchedules(logs *[]string, project string, projectDir string, projectID string) {
	if utils.IsIgnored(projectDir, PIPELINE_SCHEDULES) {
		*logs = append(*logs, "===> pipeline schedules ignored")
		return
	}
	scheds, err := utils.GetGitlabPipelineSchedules(projectID)
	if err != nil {
		log.Panicf("ERROR: unable to get pipeline schedules from api: project %s\n%s", project, err)
	}
	for _, sched := range scheds {
		*logs = append(*logs, fmt.Sprintf("===> processing pipeline schedule: %s", sched.Description))
		_, err = utils.Cmd(projectDir, "terragrunt", "import",
			fmt.Sprintf("gitlab_pipeline_schedule.project_schedules[\"%s\"]", sched.Description),
			fmt.Sprintf("%s:%d", projectID, sched.ID))
		if err != nil {
			if !strings.Contains(err.Error(), "already managed") {
				log.Panicf("ERROR: unable to run terragrunt import: project %s pipeline schedule %s\n%s", project, sched.Description, err)
			} else {
				*logs = append(*logs, "=====> pipeline schedule already imported")
			}
			continue
		}
		*logs = append(*logs, "=====> success")
	}
}
func _importMirrors(logs *[]string, project string, projectDir string, projectID string) {
	if utils.IsIgnored(projectDir, MIRRORS) {
		*logs = append(*logs, "===> mirrors ignored")
		return
	}
	mirrors, err := utils.GetGitlabMirrors(projectID)
	if err != nil {
		log.Panicf("ERROR: unable to get push mirrors from api: project %s\n%s", project, err)
	}
	for _, mirror := range mirrors {
		*logs = append(*logs, fmt.Sprintf("===> processing push mirror: %d", mirror.ID))
		_, err = utils.Cmd(projectDir, "terragrunt", "import",
			fmt.Sprintf("gitlab_project_mirror.project_push_mirrors[\"%s\"]", regexp.MustCompile("https?://.+:.+@").ReplaceAllString(mirror.URL, "")),
			fmt.Sprintf("%s:%d", projectID, mirror.ID))
		if err != nil {
			if !strings.Contains(err.Error(), "already managed") {
				log.Panicf("ERROR: unable to run terragrunt import: project %s mirror %d\n%s", project, mirror.ID, err)
			} else {
				*logs = append(*logs, "=====> push mirror already imported")
			}
			continue
		}
		*logs = append(*logs, "=====> success")
	}
}
func _importDeployKeys(logs *[]string, project string, projectDir string, projectID string) {
	if utils.IsIgnored(projectDir, DEPLOY_KEYS) {
		*logs = append(*logs, "===> deploy keys ignored")
		return
	}
	deployKeys, err := utils.GetGitlabDeployKeys(projectID)
	if err != nil {
		log.Panicf("ERROR: unable to get deploy keys from api: project %s\n%s", project, err)
	}
	for _, deployKey := range deployKeys {
		*logs = append(*logs, fmt.Sprintf("===> processing deploy key: %d", deployKey.ID))
		_, err = utils.Cmd(projectDir, "terragrunt", "import",
			fmt.Sprintf("gitlab_deploy_key.project_deploy_keys[\"%s\"]", deployKey.Title),
			fmt.Sprintf("%s:%d", projectID, deployKey.ID))
		if err != nil {
			if !strings.Contains(err.Error(), "already managed") {
				log.Panicf("ERROR: unable to run terragrunt import: project %s deploy key %d\n%s", project, deployKey.ID, err)
			} else {
				*logs = append(*logs, "=====> deploy key already imported")
			}
			continue
		}
		*logs = append(*logs, "=====> success")
	}
}

func Plan(args string) {
	logs := &([]string{})
	defer func() {
		recover()
		log.Println("\n" + strings.Join(*logs, "\n"))
	}()
	totalCreate, totalUpdate, totalDelete := 0, 0, 0
	log.Println("PLANNING", args)
	if args != "" {
		_template(args)
		totalCreate, totalUpdate, totalDelete = _plan(logs, args)
		return
	}
	changes, err := utils.GetChangedProjects()
	if err != nil {
		log.Panic(err)
	}
	if len(changes) == 0 {
		log.Println("=> no changes")
		return
	}
	for _, project := range changes {
		_template(project)
	}
	ch := make(chan interface{}, len(changes))
	for _, project := range changes {
		go func(project string) {
			crt, upd, del := _plan(logs, project)
			totalCreate += crt
			totalUpdate += upd
			totalDelete += del
			ch <- nil
		}(project)
	}
	for i := 0; i < len(changes); i++ {
		<-ch
	}
	defer func() {
		report := models.Report{
			Create: totalCreate,
			Update: totalUpdate,
			Delete: totalDelete,
		}
		out, err := json.Marshal(report)
		if err != nil {
			log.Panic(err)
		}
		err = ioutil.WriteFile(utils.GetBaseDir()+"report.json", out, 0600)
		if err != nil {
			log.Panic(err)
		}
		log.Println("REPORT SAVED:", utils.GetBaseDir()+"report.json")
	}()
}
func _plan(logs *[]string, project string) (int, int, int) {
	crt, upd, del := 0, 0, 0
	*logs = append(*logs, "=> planning: "+project)
	projectDir := utils.GetRepoDir() + project
	stdout, err := utils.Cmd(projectDir, "terragrunt", "plan", "-out=plan")
	if err != nil {
		log.Panicf("ERROR: unable to plan: project %s\n%s", project, err)
	}
	*logs = append(*logs, fmt.Sprintf("===> TERRAGRUNT PLAN START: %s\n%s===> TERRAGRUNT PLAN END: %s", project, REGEX_TERRAFORM_CLEANUP.ReplaceAllString(stdout, ""), project))
	stdout, err = utils.Cmd(projectDir, "terragrunt", "show", "--json", "plan")
	if err != nil {
		log.Panicf("ERROR: unable to show plan: project %s\n%s", project, err)
	}
	var plan models.Plan
	err = json.Unmarshal([]byte(stdout), &plan)
	if err != nil {
		log.Panicf("ERROR: invalid plan: project %s\n%s", project, err)
	}
	for _, change := range plan.ResourceChanges {
		for _, action := range change.Change.Actions {
			switch action {
			case "create":
				crt++
			case "update":
				upd++
			case "delete":
				del++
			}
		}
	}
	return crt, upd, del
}
func Apply(args string) {
	logs := &([]string{})
	defer func() {
		recover()
		log.Println("\n" + strings.Join(*logs, "\n"))
	}()
	log.Println("APPLYING", args)
	if args != "" {
		_template(args)
		_apply(logs, args)
		return
	}
	changes, err := utils.GetChangedProjects()
	if err != nil {
		log.Panic(err)
	}
	if len(changes) == 0 {
		log.Println("=> no changes")
		return
	}
	for _, project := range changes {
		_template(project)
	}
	ch := make(chan interface{}, len(changes))
	for _, project := range changes {
		go func(project string) {
			_apply(logs, project)
			ch <- nil
		}(project)
	}
	for i := 0; i < len(changes); i++ {
		<-ch
	}
}
func _apply(logs *[]string, project string) {
	*logs = append(*logs, "=> applying: "+project)
	projectDir := utils.GetRepoDir() + project
	stdout, err := utils.Cmd(projectDir, "terragrunt", "apply", "-auto-approve")
	if err != nil {
		log.Panicf("ERROR: unable to apply: project %s\n%s", project, err)
	}
	*logs = append(*logs, fmt.Sprintf("===> TERRAGRUNT APPLY START: %s\n%s===> TERRAGRUNT APPLY END: %s", project, REGEX_TERRAFORM_CLEANUP.ReplaceAllString(stdout, ""), project))
}
