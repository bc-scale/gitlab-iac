package responses

type Project struct {
	Archived         bool `json:"archived"`
	SharedWithGroups []struct {
		GroupID       int    `json:"group_id"`
		GroupFullPath string `json:"group_full_path"`
	} `json:"shared_with_groups"`
}
