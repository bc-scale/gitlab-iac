package responses

type Mirrors []struct {
	ID  int    `json:"id"`
	URL string `json:"url"`
}
