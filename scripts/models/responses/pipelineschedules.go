package responses

type PipelineSchedules []struct {
	ID          int    `json:"id"`
	Description string `json:"description"`
}
