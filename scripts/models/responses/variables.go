package responses

type Variables []struct {
	Key              string `json:"key"`
	EnvironmentScope string `json:"environment_scope"`
}
