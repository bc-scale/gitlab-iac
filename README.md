# Personal Gitlab Infrastructure-as-Code


## About
This is a Terragrunt & Terraform project that manages one's personal Gitlab account.


## Usage
### Required environment variables
|Name|Description|
|---|---|
|PROJECT_OWNER|Username of main account|
|CI_PROJECT_ID|Project ID of the IaC project|
|GITLAB_USER_TOKEN|Gitlab token with all privileges|

### Using extra environment variables in project
Enclose the variable in `${` and `}` and run `go run scripts/main.go template`

### Environment setup (Linux/Mac OS)
#### Install and Prepare Go
https://golang.org/doc/install
#### Add bin folder to PATH
```shell
BASEDIR=$(dirname "$0")
if [[ ! $PATH == *"${BASEDIR}/bin"* ]]; then
  export PATH="${BASEDIR}/bin:${PATH}"
fi
```
#### (Optional) Initialize tgenv & tfenv
```shell
tgenv install && tfenv install
```

### Commands
|Command|Description|
|---|---|
|help|show help text|
|new ([required] project-slug)|initialize project from .template
|list|list all projects|
|diff|list changed projects|
|import ([optional, blank for diff-only] project-slug)|import project to state|
|template ([optional, blank for diff-only] project-slug)|replace all environment variables in project definition|
|plan ([optional, blank for diff-only] project-slug)|run terragrunt plan|
|apply ([optional, blank for diff-only] project-slug)|run terragrunt apply|


## License
Distributed under the MIT License. See `LICENSE` for more information.


## Contact
Jaryl Chng - git@jarylchng.com

https://jarylchng.com

Project Link: [https://gitlab.com/jarylc/gitlab-iac/](https://gitlab.com/jarylc/gitlab-iac/)
