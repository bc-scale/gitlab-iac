## (optional) [boolean] Enable read-only mode. Default is false.
#archived: true

## (optional) [string] Name of the project (defaults to folder name).
#name: name

## (optional) ["public"/"internal"/"private"] Project visibility. Default is private.
# visibility_level: public

# (recommended) [string] A description of the project.
description: |-
  EXAMPLE_DESCRIPTION

## (optional) [list(string)] Tags (topics) of the project.
#tags:
#  - EXAMPLE_TAG

# (recommended) [string] The default branch for the project.
default_branch: master

# (recommended) [https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/branch_protection#argument-reference] Branch protection settings for the project.
branch_protections:
  - name: master
    push_access_level: maintainer
    merge_access_level: maintainer
    code_owner_approval_required: true # (not available on older projects)

## (optional) [list] Repository users list (project owner will be ignored)
#users:
#  - username: EXAMPLE_USERNAME # (required) [string] Gitlab username of user
#    access_level: guest # (required) ["guest"/"reporter"/"developer"/"maintainer"] Role of user.
## (optional) [list] Repository groups list
#groups:
#  - path: EXAMPLE_GROUP_NAME # (required) [string] Full path/Name of group
#    access_level: guest # (required) ["guest"/"reporter"/"developer"/"maintainer"] Role of group.

## (optional) [boolean] Enables pull mirroring in a project. Default is false.
#pull_mirror: true
## (optional) [string] Git URL to a repository to for pull-mirroring.
#pull_mirror_import_url: https://gitlab.com/jarylc/docker-tgenv-tfenv.git
## (optional) [boolean] Pull mirroring triggers builds. Default is false.
#pull_mirror_trigger_builds: true
## (optional) [boolean] Pull mirror overwrites diverged branches. Default is false.
#pull_mirror_overwrites_diverged_branches: true
## (optional) [boolean] Globally only mirror protected branches. Default is false.
#pull_mirror_only_protected_branches: true
## (optional) [https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/project_mirror] Push mirrors list.
#push_mirrors:
#  - url: https://username:${SECRETS_GITHUB_PAT}@github.com/username/repository.git
#    enabled: false
#    only_protected_branches: true
#    keep_divergent_refs: false

## (optional) [boolean] Allow users to request member access. Default is false.
#request_access_enabled: true

## (optional) [boolean] Functionality flags. Defaults are true.
#issues_enabled: false
#merge_requests_enabled: false
#pipelines_enabled: false
#wiki_enabled: false
#snippets_enabled: false
#container_registry_enabled: false
#lfs_enabled: false
#packages_enabled: false

# (recommended) ["disabled"/"private"/"enabled"/"public"/"private"] Pages access control. Default is private.
pages_access_level: public

## (optional) ["merge"/"rebase_merge"/"ff"] Merge method. "ff" stands for fast-forward merges. Default is merge.
#merge_method: merge
## (recommended) [integer] Number of merge request approvals required for merging. Default is 0.
approvals_before_merge: 1
# (recommended) [boolean] Allow merges only if a pipeline succeeds. Default is false.
only_allow_merge_if_pipeline_succeeds: true
## (optional) [boolean] Allow merges only if all discussions are resolved. Default is false.
#only_allow_merge_if_all_discussions_are_resolved: true
## (optional) [boolean] Remove all approvals in a merge request when new commits are pushed. Default is true.
# reset_approvals_on_push: false
## (optional) [boolean] Disable overriding merge requests approval rules. Default is true.
# disable_overriding_approvers_per_merge_request: false
## (optional) [boolean] Allow merge request authors to self-approve merge requests. Authors still need to be included in the approvers list. Default is true.
# merge_requests_author_approval: false
## (optional) [boolean] Prevent approval of merge requests by merge request committers. Default is false.
# merge_requests_disable_committers_approval: true
## (optional) [boolean] Enable `Delete source branch` option by default for all new merge requests. Default is true.
# remove_source_branch_after_merge: false
## (optional) [string] Squash commits when merge request. Valid values are `never`, `always`, `default_on`, or `default_off`. Default is `default_off`.
# squash_option: default_off

## (optional) [boolean] Enable shared runners for this project. Default is true.
#shared_runners_enabled: false

## (optional) [string] Custom Path to CI config file.
#ci_config_path: .gitlab-ci.yml

## (not recommended) [boolean] Initialize with a README.md file. Default is false.
#initialize_with_readme: true

# (recommended, not allowed on certain projects) [https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/project#push_rules] Push rules for the project.
push_rules:
  commit_committer_check: true
  deny_delete_tag: false
  member_check: true
  prevent_secrets: true
  reject_unsigned_commits: true
#  author_email_regex:
#  branch_name_regex:
#  commit_message_regex:
#  commit_message_negative_regex:
#  file_name_regex:

## (optional) [boolean] Use either custom instance or group (with group_with_project_templates_id) project template (enterprise edition). Default is false.
#use_custom_template: true
## (optional) [string] When used without use_custom_template, name of a built-in project template. When used with use_custom_template, name of a custom project template. This option is mutually exclusive with template_project_id.
#template_name:
## (optional) [string] When used with use_custom_template, project ID of a custom project template. This is preferable to using template_name since template_name may be ambiguous (enterprise edition). This option is mutually exclusive with template_name.
#template_project_id:
## (optional) [string] For group-level custom templates, specifies ID of group from which all the custom project templates are sourced. Leave empty for instance-level templates. Requires use_custom_template to be true (enterprise edition).
#group_with_project_templates_id:

## (optional) [https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/project_variable] Variables list for the project.
#variables:
#  - key: EXAMPLE_VARIABLE_KEY
#    value: ${EXAMPLE_ENV_VARIABLE}
#    masked: true
#    protected: true
#    environment_scope: "*"

## (optional) [https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/pipeline_schedule#argument-reference] Pipeline schedule list for the project.
#schedules:
#  - description: EXAMPLE_DESCRIPTION
#    ref: master
#    cron: 0 6 * * *
#    cron_timezone: Asia/Singapore
#    active: true
#    # (optional) [https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/pipeline_schedule_variable#argument-reference] Variables for this schedule.
#    variables:
#      - key: EXAMPLE_KEY
#        value: EXAMPLE_VALUE

## (optional) [https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/pipeline_trigger] Pipeline triggers for the project.
#triggers:
#  - description: EXAMPLE_DESCRIPTION

## (optional) [https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/project_hook#argument-reference] Webhooks list for the project.
#hooks:
#  - url: "https://example.com/hook"
#    token: EXAMPLE_TOKEN
#    enable_ssl_verification: true
#    push_events: true
#    push_events_branch_filter: master
#    issues_events: true
#    confidential_issues_events: true
#    merge_requests_events: true
#    tag_push_events: true
#    note_events: true
#    confidential_note_events: true
#    job_events: true
#    pipeline_events: true
#    wiki_page_events: true
#    deployment_events: true

## (optional) [https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/deploy_key#argument-reference] Deploy keys for the project.
#deploy_keys:
#  - title: EXAMPLE_TITLE
#    key: ssh-rsa AAAA...
#    can_push: true

## (not in use) [https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/deploy_key#argument-reference] Deploy tokens for the project.
#deploy_tokens:
#  - name: EXAMPLE_NAME
#    scopes: [ "read_repository", "read_registry" ]
#    expires_at: 'YYYY-MM-DDTHH:MM:SS+HO:MO'

## [https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/service_github#argument-reference] Sync project pipeline status with GitHub
#github_integration:
#  token: ${SECRETS_GITHUB_PAT} # requires `repo:status` scope
#  path: username/repository
#  static_context: false

## (optional) [list] Self pipeline triggers for the project. Combination of creating a trigger and a hook pointing to it.
#self_triggers:
#  - branch: ci
#    description: EXAMPLE_DESCRIPTION
#    push_events: true
#    push_events_branch_filter: master
#    issues_events: true
#    confidential_issues_events: true
#    merge_requests_events: true
#    tag_push_events: true
#    note_events: true
#    confidential_note_events: true
#    job_events: true
#    pipeline_events: true
#    wiki_page_events: true
#    deployment_events: true
